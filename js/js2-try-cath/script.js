const root = document.querySelector('#root')
const list = document.createElement('ul')


const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];




root.append(list)
books.forEach((i) => {
    try {
        if (i.hasOwnProperty('author') === false) {
            throw Error('No author in ' + i.name)
        } else if (i.hasOwnProperty('name') === false) {
            throw Error('No name in ' + i.name)
        } else if (i.hasOwnProperty('price') === false) {
            throw Error('No price in ' + i.name)
        } else {
            let it = document.createElement('li')
            it.innerText = 'Название: ' + i.name + '. Автор: ' + i.author + '. Цена: ' + i.price
            list.append(it)
        }
    } catch (e) {
        console.error(e)
    }
})
