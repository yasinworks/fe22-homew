class Employee {
    constructor(name, age, salary) {
        this.name = name
        this.age = age
        this.salary = salary
    }

    get EmployeeGetSalary() {
        return this.salary
    }

    set EmployeeSetSalary(sal) {
        return this.salary = sal
    }

    get EmployeeGetName() {
        return this.name
    }

    set EmployeeGetSame(name) {
        return this.name = name
    }

    get EmployeeGetAge() {
        return this.age
    }

    set EmployeeSetAge(age) {
        return this.age = age
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);

        this.lang = lang
    }

    get progGetLang() {
        return this.lang
    }

    set progSetLang(lang) {
        return this.lang
    }

    get progGetSalary() {
        return this.salary * 3
    }

    set progSetSalary(sal) {
        return this.salary = sal
    }
}

const e = new Employee('Geog', 31, 20000)
const p = new Programmer('Geog', 31, 2000, 'C++, js')

console.log(p.progGetSalary)