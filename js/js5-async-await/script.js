const getIPBtn = document.getElementById('ip')


getIPBtn.addEventListener('click', () => {
    getIP()
})

async function getIP() {
    await fetch('https://api.ipify.org/?format=json')
        .then(r => r.json())
        .then(ipJSON => getInfo(ipJSON.ip))
}

async function getInfo(ip) {
    await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,region,city,district&lang=ru`)
        .then(r => r.json())
        .then(ipJSON => placeInfo(ipJSON.continent ,ipJSON.country, ipJSON.region, ipJSON.city, ipJSON.district))
}

async function placeInfo(continent ,country, region, city, district,) {
    let parent = document.getElementById('1')
    Array.from(arguments).forEach((e) => {
        let text = document.createElement('span')
        text.innerText = e
        parent.append(text)
    })

}