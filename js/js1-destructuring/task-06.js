const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const improvedEmployee = {...employee}

const {age: age = 50, salary: salary = 1000} = improvedEmployee


// ALSO WORKS
// improvedEmployee.age = 50;
// improvedEmployee.salary = 1000;
//
// improvedEmployee['age'] = 50;
// improvedEmployee['salary'] = 1000;


console.log(improvedEmployee)