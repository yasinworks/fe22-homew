fetch('https://swapi.dev/api/films/', {method: 'GET'})
    .then(resp => resp.json())
    .then(filmData => getFilms(filmData))


function getFilms(all) {

    all.results.forEach(film => {

        let p = document.createElement('div')
        let list = document.createElement('ul')
        p.className = 'film'
        list.className = 'film-characters'
        p.append(list)
        document.body.append(p)

        for (let i = 1; i <= all.count; i++) {
            if (i === film.episode_id) {
                p.insertAdjacentHTML('afterbegin', `<h3 class="film-title">${film.title}, number - ${film.episode_id}</h3> <span class="film-desc">${film.opening_crawl}</span>`)
            }
        }

        film.characters.forEach(character => {

            fetch(character, {method: 'GET'})
                .then(r => r.json())
                .then(d => getCharacter(d))

            function getCharacter(characters) {

                characters.films.forEach(() => {
                    fetch(character, {method: 'GET'})
                        .then(r => r.json())
                        .then(char => {

                            if (p.innerText.indexOf(char.name) > -1) {
                            } else {
                                let charSpan = document.createElement('li')
                                charSpan.innerText = char.name
                                list.append(charSpan)
                            }
                        })
                })
            }
        })
    })
}