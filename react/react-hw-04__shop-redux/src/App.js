import React, {useState, useEffect} from 'react';
import AppRoutes from "./routes/AppRoutes";
import NavBar from "./components/NavBar/NavBar";
import {removeFromCart, addCart, toggleFavorite, getProducts} from "./store/operations";
import {useDispatch} from "react-redux";

function App() {
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(getProducts())
    }, [])
    return (
        <div>
            <NavBar/>
            <AppRoutes removeFromCart={removeFromCart} addCart={addCart} toggleFavorite={toggleFavorite}/>
        </div>
    )
}


export default App;
