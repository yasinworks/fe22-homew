import React from 'react';
import Favorite from "../Favorite/Favorite";
import PropTypes from 'prop-types';
import './product.scss'
import {useDispatch, useSelector} from "react-redux";
import {openModal, openRemoveItemModal} from "../../store/operations";

function Product(props) {
    const {item, btnCart, deleteCartBtn, toggleFavorite, addCart, removeFromCart} = props
    const modalOpened = useSelector(state => state.addModal)
    const removeModal = useSelector(state => state.removeModal)
    const prodVendor = useSelector(state => state.prodVendor)
    const dispatch = useDispatch()

    const styles = {
        actionButton: {
            color: 'white',
            margin: '5px'
        },
        svg: {
            width: '40px',
            height: '40px',
            position: 'absolute',
            top: '10px',
            right: '10px'
        }
    }



    return (
        <div className={'flex'}>
            <div className={'product'}>
                <div>
                    <img className={'img'} src={item.img} alt="img"/>
                </div>
                <Favorite isFavorite={item.isFavorite} styles={styles.svg} onClick={() => dispatch(toggleFavorite(item.vendorCode))} />
                {deleteCartBtn && <button onClick={() => dispatch(openRemoveItemModal(item.vendorCode))} className={'product-remove'}>
                    <img src="/cartremove.png" alt=""/>
                </button>}
                <h3>{item.name}</h3>
                <span className={'product-description'}>Цена: {item.price}₴</span>
                <span className={'product-description'}>Артикул: {item.vendorCode}</span>
                <span className={'product-description'}>Цвет: {item.color}</span>
                {removeFromCart && <span className={'product-description'}>Количество: {item.inCart}</span>}
                {btnCart && <button className={'product-button'} onClick={() => dispatch(openModal(item.vendorCode))}>Добавить в корзину</button>}
            </div>


            <div>
                {/*{modalOpened && <Modal*/}
                {/*    modalText={`Добавить в корзину ${item.name}?`}*/}
                {/*    title={'Вопрос'}*/}
                {/*    buttonOnClick={() => hideModal()}*/}
                {/*    closeBtn*/}
                {/*    actions={{*/}
                {/*        cancel: () => <Button cStyles={styles.actionButton} text={'Close'} bgc={'darkred'}*/}
                {/*                              OnClick={() => dispatch(hideModal())}/>,*/}
                {/*        ok: () => <Button cStyles={styles.actionButton} text={'Ok'} bgc={'darkgreen'}*/}
                {/*                          OnClick={() => dispatch(addCart(item.vendorCode))}/>,*/}
                {/*    }}*/}
                {/*/>}*/}

                {/*{removeModal && <Modal*/}
                {/*    modalText={`Удалить из корзины ${item.name}?`}*/}
                {/*    title={'Вопрос'}*/}
                {/*    buttonOnClick={() => hideModal()}*/}
                {/*    closeBtn*/}
                {/*    actions={{*/}
                {/*        cancel: () => <Button cStyles={styles.actionButton} text={'Close'} bgc={'darkred'}*/}
                {/*                              OnClick={() => dispatch(hideRemoveItemModal())}/>,*/}
                {/*        ok: () => <Button cStyles={styles.actionButton} text={'Ok'} bgc={'darkgreen'}*/}
                {/*                          OnClick={() => dispatch(removeFromCart(item.vendorCode))}/>,*/}
                {/*    }}*/}
                {/*/>}*/}

            </div>
        </div>
    )
}

Product.propTypes = {
    item: PropTypes.object
};

export default Product;