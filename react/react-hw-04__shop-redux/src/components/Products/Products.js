import React from 'react';
import Product from "../Product/Product";
import PropTypes from 'prop-types';
import './products.scss'
import Modal from "../Modal/Modal";
import {hideModal, hideRemoveItemModal} from "../../store/operations";
import Button from "../Button/Button";
import {useDispatch, useSelector} from "react-redux";

function Products({products, btnCart, deleteCartBtn, toggleFavorite, addCart, removeFromCart}) {
    const modalOpened = useSelector(state => state.addModal)
    const removeModal = useSelector(state => state.removeModal)
    const prodVendor = useSelector(state => state.prodVendor)
    const dispatch = useDispatch()

    const styles = {
        actionButton: {
            color: 'white',
            margin: '5px'
        }
    }

    return (
        <div className={'products'}>
            {products && products.map((item) => <Product removeFromCart={removeFromCart} addCart={addCart} toggleFavorite={toggleFavorite} deleteCartBtn={deleteCartBtn} btnCart={btnCart} item={item}/>)}
            <div>
                {modalOpened && <Modal
                    modalText={`Добавить в корзину ?`}
                    title={'Вопрос'}
                    buttonOnClick={() => hideModal()}
                    closeBtn
                    actions={{
                        cancel: () => <Button cStyles={styles.actionButton} text={'Close'} bgc={'darkred'}
                                              OnClick={() => dispatch(hideModal())}/>,
                        ok: () => <Button cStyles={styles.actionButton} text={'Ok'} bgc={'darkgreen'}
                                          OnClick={() => dispatch(addCart(prodVendor))}/>,
                    }}
                />}

                {removeModal && <Modal
                    modalText={`Удалить из корзины?`}
                    title={'Вопрос'}
                    buttonOnClick={() => hideModal()}
                    closeBtn
                    actions={{
                        cancel: () => <Button cStyles={styles.actionButton} text={'Close'} bgc={'darkred'}
                                              OnClick={() => dispatch(hideRemoveItemModal())}/>,
                        ok: () => <Button cStyles={styles.actionButton} text={'Ok'} bgc={'darkgreen'}
                                          OnClick={() => dispatch(removeFromCart(prodVendor))}/>,
                    }}
                />}
            </div>
        </div>
    )
}

Products.propTypes = {
    products: PropTypes.array,
    onClick: PropTypes.func
};

export default Products;