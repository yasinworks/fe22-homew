import React from 'react';
import './button.scss'

function Button(props) {
    const {text, OnClick, bgc, cStyles} = props
    return (
        <button className={'button'} style={{backgroundColor: bgc, ...cStyles}} onClick={OnClick}>{text}</button>
    )
}

export default Button;