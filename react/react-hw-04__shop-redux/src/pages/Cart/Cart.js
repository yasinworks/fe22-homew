import React from 'react';
import Products from "../../components/Products/Products";
import {useSelector} from "react-redux";

function Cart({btnCart, deleteCartBtn, toggleFavorite, removeFromCart}) {
    const products = useSelector(state => state.products.data)
    let productsArray = null
    let favoriteProducts = null
    if (products) {
        productsArray = products
        favoriteProducts = productsArray.filter(item => item.inCart > 0)
    }

    return (
        <div>
            {products ? <Products removeFromCart={removeFromCart} toggleFavorite={toggleFavorite} deleteCartBtn={deleteCartBtn} btnCart={btnCart} products={favoriteProducts}/> : <h1>Empty</h1>}
        </div>
    );
}

export default Cart;