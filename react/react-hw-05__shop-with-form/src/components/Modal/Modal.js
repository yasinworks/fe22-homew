import React, {Component} from 'react';
import Button from "../Button/Button";
import PropTypes from 'prop-types'
import './modal.scss'

function Modal(props) {
    const {modalText, title, buttonOnClick, closeBtn, actions} = props

    const styles = {
        button: {
        padding: '1px',
        fontSize: '32px',
        color: 'white'
    }
    }
    return (
        <div onClick={buttonOnClick} id={'wrapper'} className={'modalWrapper'}>
            <div className={'modal'}>
                <div className={'header'}>
                    <h3 className={'modalTitle'}>{title}</h3>
                    {closeBtn &&
                    <Button text={'x'} bgc={'transparent'} cStyles={styles.button} OnClick={buttonOnClick}/>}
                </div>
                <div className={'modalBody'}>
                    <div className={'modalText'}>
                        {modalText}
                    </div>
                </div>
                <div className={'modalFooter'}>
                    {actions.cancel && actions.cancel()}
                    {actions.ok && actions.ok()}
                </div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    modalText: PropTypes.string,
    title: PropTypes.string,
    buttonOnClick: PropTypes.func,
    closeBtn: PropTypes.bool,
    actions: PropTypes.object
}

export default Modal;