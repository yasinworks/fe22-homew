import {
    CART_MODAL_HIDDEN,
    CART_MODAL_OPENED, CLIENT_CHECKOUT,
    LOAD_PRODUCTS_SUCCESS, MODAL_HIDDEN,
    MODAL_OPENED,
    PRODUCT_ADD_TO_CART,
    PRODUCT_REMOVE_FROM_CART, SELECT_PRODUCTS,
    TOGGLE_FAVORITE
} from "./types";
import Modal from "../components/Modal/Modal";

const initialState = {
    products: {
        data: [],
        isLoading: true
    },
    client: {},
    addModal: false,
    removeModal: false,
    prodVendor: null
}

const reducer = (state = initialState, action) => {
    let products = state.products.data
    switch (action.type) {
        case LOAD_PRODUCTS_SUCCESS:
            const productsArray = action.payload.map(el => {
                el.isFavorite = false
                el.inCart = 0
                return el
            })
            return {...state, products: {...state.products, data: productsArray, isLoading: false}}

        case TOGGLE_FAVORITE:
            const newArray = products.map(el => {
                if (el.vendorCode === action.payload) {
                    el.isFavorite = !el.isFavorite
                }
                return el
            })
            return {...state, products: {...state.products, data: newArray}}

        case PRODUCT_ADD_TO_CART:
            const addedToCart = products.map(el => {
                if (el.vendorCode === action.payload) {
                    el.inCart = el.inCart + 1
                }
                return el
            })
            return {...state, products: {...state.products, data: addedToCart}, addModal: false}

        case PRODUCT_REMOVE_FROM_CART:
            const removedFromCart = products.map(el => {
                if (el.vendorCode === action.payload) {
                    el.inCart = 0
                }
                return el
            })
            return {...state, products: {...state.products, data: removedFromCart}, removeModal: false}

        case MODAL_OPENED:
            return {...state, addModal: action.payload}

        case MODAL_HIDDEN:
            return {...state, addModal: action.payload}

        case CART_MODAL_OPENED:
            return {...state, removeModal: action.payload}

        case CART_MODAL_HIDDEN:
            return {...state, removeModal: action.payload}

        case SELECT_PRODUCTS:
            console.log('1', action.payload)
            return {...state, prodVendor: action.payload}

        case CLIENT_CHECKOUT:
            return {...state, client: action.payload}

        default:
            return state
    }
}

export default reducer