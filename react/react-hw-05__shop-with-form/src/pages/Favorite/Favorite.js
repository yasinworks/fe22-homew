import React from 'react';
import Products from "../../components/Products/Products";
import {useSelector} from "react-redux";

function Favorite(props) {
    const {btnCart, addCart} = props
    const products = useSelector(state => state.products.data)

    let favoriteProducts = null
    if (products) {
        favoriteProducts = products.filter(item => item.isFavorite)
    }
        console.log(favoriteProducts)




    return (
        <div>
            {favoriteProducts ? <Products btnCart={btnCart} addCart={addCart} toggleFavorite={props.toggleFavorite} products={favoriteProducts}/> : <h3>Empty</h3>}
        </div>
    );
}

export default Favorite;