import React, {useEffect} from 'react';
import {Route, Switch, Redirect} from 'react-router-dom'
import Favorite from "../pages/Favorite/Favorite";
import Products from "../components/Products/Products";
import Cart from "../pages/Cart/Cart";
import {getProducts} from "../store/operations";
import MainProducts from "../pages/MainProducts/MainProducts";

function AppRoutes(props) {
    useEffect(() => {
        getProducts()
    }, [])
    return (
        <div>
            <Switch>
                <Redirect exact from={'/'} to={'/products'} />
                <Route exact path={'/products'} render={() => <MainProducts addCart={props.addCart} toggleFavorite={props.toggleFavorite} btnCart />} />
                <Route exact path={'/favorites'} render={() => <Favorite addCart={props.addCart} toggleFavorite={props.toggleFavorite} products={props.products} btnCart/>} />
                <Route exact path={'/cart'} render={() => <Cart removeFromCart={props.removeFromCart} toggleFavorite={props.toggleFavorite} products={props.products} deleteCartBtn btnCart={false} />} />
            </Switch>
        </div>
    );
}

export default AppRoutes;