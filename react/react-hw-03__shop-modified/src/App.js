import React, {useState, useEffect} from 'react';
import axios from 'axios'
import AppRoutes from "./routes/AppRoutes";
import NavBar from "./components/NavBar/NavBar";

function App() {
    const [products, setProducts] = useState()

    const getProducts = () => {
        axios.get('/products.json')
            .then(data => {
                const newArray = data.data.map(el => {
                    el.isFavorite = false
                    el.inCart = 0
                    return el
                })
                setProducts(newArray)
            })
    }

    useEffect(() => {
        getProducts()
    }, [])


    const toggleFavorite = (vendor) => {
        const newArray = products.map(el => {
            if (el.vendorCode === vendor) {
                el.isFavorite = !el.isFavorite
            }
            return el
        })
        setProducts(newArray)
    }

    const addCart = (vendor) => {
        const newArray = products.map(el => {
            if (el.vendorCode === vendor) {
                el.inCart = el.inCart + 1
            }
            return el
        })
        setProducts(newArray)
        console.log('addCart - ',newArray)
    }

    const removeFromCart = (vendor) => {
        const newArray = products.map(el => {
            if (el.vendorCode === vendor) {
                el.inCart = 0
            }
            return el
        })
        setProducts(newArray)
    }

    return (
        <div>
            <NavBar/>
            <AppRoutes removeFromCart={removeFromCart} addCart={addCart} toggleFavorite={toggleFavorite} products={products}/>
        </div>
    )
}


export default App;
