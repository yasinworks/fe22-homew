import React from 'react';
import Products from "../../components/Products/Products";

function Favorite(props) {
    const {products, btnCart, addCart} = props
    let favoriteProducts;
    if (products) {
        favoriteProducts = products.filter(item => item.isFavorite)
    }




    return (
        <div>
             <Products btnCart={btnCart} addCart={addCart} toggleFavorite={props.toggleFavorite} products={favoriteProducts}/> 
        </div>
    );
}

export default Favorite;