import React from 'react'
import Products from '../../components/Products/Products'


export default function Main(props) {
    return (
        <React.Fragment>
            <Products addCart={props.addCart} toggleFavorite={props.toggleFavorite} products={props.products} btnCart />
        </React.Fragment>
    )
}
