import React from 'react';
import Products from "../../components/Products/Products";

function Cart({btnCart, deleteCartBtn, products, toggleFavorite, removeFromCart}) {
    let productsArray;
    let favoriteProducts;
    if (products) {
        productsArray = products
        favoriteProducts = productsArray.filter(item => item.inCart > 0)
    }

    return (
        <div>
            <Products removeFromCart={removeFromCart} toggleFavorite={toggleFavorite} deleteCartBtn={deleteCartBtn} btnCart={btnCart} products={favoriteProducts}/>
        </div>
    );
}

export default Cart;