import React, {} from 'react';
import Product from "../Product/Product";
import PropTypes from 'prop-types';
import './products.scss'

function Products({products, onClick, btnCart, deleteCartBtn, toggleFavorite, addCart, removeFromCart}) {
    return (
        <div className={'products'}>
            {products && products.map((item) => <Product key={item.vendorCode} removeFromCart={removeFromCart} addCart={addCart} toggleFavorite={toggleFavorite} deleteCartBtn={deleteCartBtn} btnCart={btnCart} onClick={onClick} item={item}/>)}
        </div>
    )
}

Products.propTypes = {
    products: PropTypes.array,
    onClick: PropTypes.func
};

export default Products;