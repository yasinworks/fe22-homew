import React, { useState } from "react";
import { useLocation } from "react-router-dom";
import Favorite from "../Favorite/Favorite";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import "./product.scss";

function Product(props) {
  const {
    item,
    btnCart,
    deleteCartBtn,
    toggleFavorite,
    addCart,
    removeFromCart,
  } = props;
  const [modal, setModal] = useState();
  let history = useLocation();

  const styles = {
    actionButton: {
      color: "white",
      margin: "5px",
    },
    svg: {
      width: "40px",
      height: "40px",
      position: "absolute",
      top: "10px",
      right: "10px",
    },
  };

  return (
    <div className="flex">
      <div className="product">
        <div>
          <img className="img" src={item.img} alt="img" />
        </div>
        <Favorite
          isFavorite={item.isFavorite}
          styles={styles.svg}
          onClick={() => toggleFavorite(item.vendorCode)}
        />
        {deleteCartBtn && (
          <button onClick={() => setModal(true)} className={"product-remove"}>
            <img src="/cartremove.png" alt="" />
          </button>
        )}
        <h3>{item.name}</h3>
        <span className="product-description">Цена: {item.price}₴</span>
        <span className="product-description">
          Артикул: {item.vendorCode}
        </span>
        <span className="product-description">Цвет: {item.color}</span>
        {removeFromCart && (
          <span className="product-description">
            Количество: {item.inCart}
          </span>
        )}
        {btnCart && (
          <button className="product-button" onClick={() => setModal(true)}>
            Добавить в корзину
          </button>
        )}
      </div>

      <div>
        {modal && (
          <Modal
            modalText={() => {
              if (
                history.pathname === "/products" ||
                history.pathname === "/favorites"
              ) {
                return `Добавить в корзину ${item.name}?`;
              } else {
                return `Удалить из корзины ${item.name}?`;
              }
            }}
            title={"Вопрос"}
            buttonOnClick={() => setModal(false)}
            closeBtn
            actions={{
              cancel: () => (
                <Button
                  cStyles={styles.actionButton}
                  text="Close"
                  bgc="darkred"
                  OnClick={() => setModal(false)}
                />
              ),
              ok: () => (
                <Button
                  cStyles={styles.actionButton}
                  text={"Ok"}
                  bgc={"darkgreen"}
                  OnClick={() => {
                    if (
                      history.pathname === "/products" ||
                      history.pathname === "/favorites"
                    ) {
                      return addCart(item.vendorCode);
                    } else {
                      return removeFromCart(item.vendorCode);
                    }
                  }}
                />
              ),
            }}
          />
        )}
      </div>
    </div>
  );
}

Product.propTypes = {
  item: PropTypes.object,
};

export default Product;
