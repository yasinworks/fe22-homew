import React from 'react';
import {Route, Switch, Redirect} from 'react-router-dom'
import Favorite from "../pages/Favorite/Favorite";
import Main from "../pages/Main/Main";
import Cart from "../pages/Cart/Cart";

function AppRoutes(props) {
    return (
        <div>
            <Switch>
                <Redirect exact from='/' to='/products' />
                <Route exact path='/products' render={() => <Main addCart={props.addCart} toggleFavorite={props.toggleFavorite} products={props.products} btnCart /> } />
                <Route exact path='/favorites' render={() => <Favorite addCart={props.addCart} toggleFavorite={props.toggleFavorite} products={props.products} btnCart/>} />
                <Route exact path='/cart' render={() => <Cart removeFromCart={props.removeFromCart} toggleFavorite={props.toggleFavorite} products={props.products} deleteCartBtn btnCart={false} />} />
            </Switch>
        </div>
    );
}

export default AppRoutes;