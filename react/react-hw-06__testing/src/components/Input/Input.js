import { useField } from "formik";
import React from "react";
import './input.scss'

const Input = (props) => {
    const {type, label, name} = props
    const [field, meta] = useField(name);
    return (
        <div>
            <div>
                <label>
                    <h5 className={'input-title'}>{label}</h5>
                    <input className={'input'} type={type}  {...field} />
                </label>
            </div>
            {meta.error && meta.touched && (
                <span className="input-error">{meta.error}</span>
            )}
        </div>
    );
};

export default Input;
