import React from 'react';
import './button.scss'

function Button(props) {
    const {text, OnClick, bgc = 'white', cStyles} = props

    if(cStyles) {
        return <button className={'button'} style={{backgroundColor: bgc, ...cStyles}} onClick={OnClick}>{text}</button>
    } else {
        return <button className={'button'} style={{backgroundColor: bgc}} onClick={OnClick}>{text}</button>
    }
}

export default Button;