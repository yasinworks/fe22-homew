import React from 'react';
import './navbar.scss'
import {NavLink} from "react-router-dom";

function NavBar(props) {
    return (
        <nav className={'nav'}>
            <NavLink to={'/products'} className={'nav-item'}>home</NavLink>
            <NavLink to={'/favorites'} className={'nav-item'}>favorite</NavLink>
            <NavLink to={'/cart'} className={'nav-item'}>cart</NavLink>
        </nav>
    );
}

export default NavBar;