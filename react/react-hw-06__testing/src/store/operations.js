import axios from "axios";
import {
    CART_MODAL_HIDDEN,
    CART_MODAL_OPENED,
    LOAD_PRODUCTS_SUCCESS, MODAL_HIDDEN,
    MODAL_OPENED,
    PRODUCT_ADD_TO_CART,
    PRODUCT_REMOVE_FROM_CART, SELECT_PRODUCTS,
    TOGGLE_FAVORITE,
    CLIENT_CHECKOUT
} from './types'

export const getProducts = () => (dispatch) => {
    axios('/products.json')
        .then(data => dispatch({type: LOAD_PRODUCTS_SUCCESS, payload: data.data}))
}

export const toggleFavorite = (vendor) => (dispatch) => {
    dispatch({type: TOGGLE_FAVORITE, payload: vendor})
}

export const addCart = (vendor) => (dispatch) => {
    dispatch({type: PRODUCT_ADD_TO_CART, payload: vendor})
}

export const removeFromCart = (vendor) => (dispatch) => {
    dispatch({type: PRODUCT_REMOVE_FROM_CART, payload: vendor})
}

export const openModal = (vendor) => (dispatch) => {
    dispatch({type: SELECT_PRODUCTS, payload: vendor})
    dispatch({type: MODAL_OPENED, payload: true})
}

export const openRemoveItemModal = (vendor) => (dispatch) => {
    dispatch({type: SELECT_PRODUCTS, payload: vendor})
    dispatch({type: CART_MODAL_OPENED, payload: true})
}

export const hideModal = () => (dispatch) => {
    dispatch({type: MODAL_HIDDEN, payload: false})
}

export const hideRemoveItemModal = () => (dispatch) => {
    dispatch({type: CART_MODAL_HIDDEN, payload: false})
}

export const checkout = (client) => (dispatch) => {
    dispatch({type: CLIENT_CHECKOUT, payload: client})
}