import React from 'react';
import Products from "../../components/Products/Products";
import {useDispatch, useSelector} from "react-redux";
import * as Yup from 'yup'
import {Formik, Form} from "formik";
import Input from "../../components/Input/Input";
import './cart.scss'
import {checkout} from "../../store/operations";

function Cart({btnCart, deleteCartBtn, toggleFavorite, removeFromCart}) {
    const dispatch = useDispatch()
    const products = useSelector(state => state.products.data)
    let productsArray = null
    let favoriteProducts = null
    if (products) {
        productsArray = products
        favoriteProducts = productsArray.filter(item => item.inCart > 0)
    }

    const submitForm = (values) => {
        const {name, surname, age, address, phoneNumber} = values
        dispatch(checkout(values))
        console.log('client name - ', name)
        console.log('client surname - ', surname)
        console.log('client age - ', age)
        console.log('client address - ', address)
        console.log('client number - ', phoneNumber)
        console.log('order - ', favoriteProducts)
    }

    const validationSchema = Yup.object().shape({
        name: Yup.string()
            .required('This field is required')
            .min(1, 'Too short'),
        surname: Yup.string()
            .required('This field is required')
            .min(1, 'Too short'),
        age: Yup.number()
            .required('This field is required'),
        address: Yup.string()
            .required('This field is required'),
        phoneNumber: Yup.string()
            .required('This field is required')
    })

    return (
        <div className={'parent'}>
            <div className={'products'}>
                {products ? <Products removeFromCart={removeFromCart} toggleFavorite={toggleFavorite}
                                      deleteCartBtn={deleteCartBtn} btnCart={btnCart} products={favoriteProducts}/> :
                    <h1>Empty</h1>}
            </div>
            <div className={'form'}>
                <Formik
                    initialValues={{
                        name: '',
                        surname: '',
                        age: '',
                        address: '',
                        phoneNumber: ''
                    }}
                    onSubmit={submitForm}
                    validationSchema={validationSchema}>

                    {formikProps => {
                        console.log(formikProps)
                        return (
                            <Form>
                                <Input name="name" type="text" label="Name"/>
                                <Input name="surname" type="text" label="Surname"/>
                                <Input name="age" type="text" label="Age"/>
                                <Input name="address" type="text" label="Address"/>
                                <Input name="phoneNumber" type="text" label="Phone number"/>

                                <div>
                                    <button className={'form-button'} type="submit">Checkout</button>
                                </div>
                            </Form>)
                    }}

                </Formik>
            </div>
        </div>
    );
}

export default Cart;