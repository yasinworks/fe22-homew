import React from 'react';
import Products from "../../components/Products/Products";
import {useSelector} from "react-redux";
import './main-prod.scss'

function MainProducts(props) {
    const {btnCart, deleteCartBtn, toggleFavorite, addCart, removeFromCart} = props
    const products = useSelector(state => state.products.data)

    return (
        <div className={'main'}>
            <Products products={products} btnCart={btnCart} deleteCartBtn={deleteCartBtn} toggleFavorite={toggleFavorite} addCart={addCart} removeFromCart={removeFromCart} />
        </div>
    );
}

export default MainProducts;