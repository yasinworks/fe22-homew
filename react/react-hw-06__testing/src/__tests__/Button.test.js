import { render } from "@testing-library/react"
import Button from "../components/Button/Button"


describe('Button testing' ,() => {
    test('Smoke test Button.js', () => {
        render( <Button text='text' onClick={jest.fn()} />)
    })

    test('Button text should be equal to Ok', () => {
        const {getByText} = render(<Button text="Ok" onClick={jest.fn()}/>)
        expect(getByText('Ok')).toBeInTheDocument()
    })

    test('Button background color should be red', () => {
        const {getByText} = render(<Button text="test button" bgc='red' onClick={jest.fn()}/>)

        const button = getByText('test button')
        const style = button.style.backgroundColor

        expect(style).toBe('red')
    })

    test('Button backgroud color should be white', () => {
        const {getByText} = render(<Button text="test button" onClick={jest.fn()}/>)  

        const button = getByText('test button')
        const style = button.style.backgroundColor

        expect(style).toBe('white')
    })
})