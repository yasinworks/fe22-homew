import { render, screen } from '@testing-library/react'
import Modal from "../components/Modal/Modal"


describe('testing Modal.js', () => {
    test('Smoke test Modal.js', () => {
        render(
            <Modal
                    modalText={`Добавить в корзину ?`}
                    title={'Вопрос'}
                    buttonOnClick={() => jest.fn()}
                    closeBtn
                    actions= {{
                        cancel: () => jest.fn(), 
                        ok: () => jest.fn()
                    }}
                    />
        )
    })

    test('Modal title should be Test Title', () => {
        const {getByText} = render(<Modal title={'Test Title'} modalText='Test text' buttonOnClick={() => jest.fn()} closeBtn actions= {{
            cancel: () => jest.fn(), 
            ok: () => jest.fn()
        }}/> )

        expect(getByText('Test Title')).toBeInTheDocument()    
    })

    test('Modal text should be Test text', () => {
        const {getByText} = render(<Modal title={'Test Title'} modalText='Test text' buttonOnClick={() => jest.fn()} closeBtn actions= {{
            cancel: () => jest.fn(), 
            ok: () => jest.fn()
        }}/> )

        expect(getByText('Test text')).toBeInTheDocument()
    })

    test('Modal should contain close button', () => {
        render(<Modal title={'Test Title'} modalText='Test text' buttonOnClick={() => jest.fn()} closeBtn actions= {{
            cancel: () => jest.fn(), 
            ok: () => jest.fn()
        }}/> )

        expect(screen.getByText('x')).toBeInTheDocument()
    })
})