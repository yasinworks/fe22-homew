import React, {Component} from 'react';
import Button from "../Button/Button";

class Modal extends Component {
    styles = {
        modalWrapper: {
            backgroundColor: 'rgba(0,0,0,0.2)',
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
        },

        modal: {
            position: 'absolute',
            left: '50%',
            top: '50%',
            backgroundColor: 'red',
            transform: 'translate(-50%, -50%)',
            borderRadius: '10px',
        },

        header: {
            display: 'flex',
            backgroundColor: 'darkred',
            justifyContent: 'space-between',
            padding: '10px',
            color: 'white',
            borderTopLeftRadius: '10px',
            borderTopRightRadius: '10px'
        },

        modalBody: {
            padding: '30px',
            borderBottomLeftRadius: '10px',
            borderBottomRightRadius: '10px'
        },

        button: {
            padding: '1px',
            fontSize: '40px',
            color: 'white'
        },

        modalTitle: {
            // margin: '10px 0 0 15px'
        },

        modalText: {
            // margin: '30px'
            color: 'white'
        },

        modalFooter: {
            display: 'flex',
            justifyContent: 'center',
            padding: '5px'
        }
    }


    render() {
        const {modalText, title, buttonOnClick, closeBtn, actions} = this.props

        return (
            <div id={'wrapper'} style={this.styles.modalWrapper}>
                <div onClick={buttonOnClick} style={this.styles.modal}>
                    <div style={this.styles.header}>
                        <h3 style={this.styles.modalTitle}>{title}</h3>
                        {closeBtn &&
                        <Button text={'x'} bgc={'transparent'} cStyles={this.styles.button} OnClick={buttonOnClick}/>}
                    </div>
                    <div style={this.styles.modalBody}>
                        <div style={this.styles.modalText}>
                            {modalText}
                        </div>
                    </div>
                    <div style={this.styles.modalFooter}>
                        {actions.cancel && actions.cancel()}
                        {actions.ok && actions.ok()}
                    </div>
                </div>
            </div>
        );
    }
}


export default Modal;