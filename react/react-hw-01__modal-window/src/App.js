import React, {Component} from 'react';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalOpened: null
        }
    }

    styles = {
        actionButton: {
            color: 'white',
            margin: '5px'
        },
    }

    hideModal() {
        this.setState({modalOpened: null})
    }

    openFirst() {
        this.setState({modalOpened: 'first'})
    }

    openSecond() {
        this.setState({modalOpened: 'second'})
    }

    render() {

        return (
            <div>
                <Button bgc={'gray'} text={'open first modal'} OnClick={() => this.openFirst()}/>
                <Button bgc={'darkgray'} text={'open second modal'}
                        OnClick={() => this.openSecond()}/>
                {this.state.modalOpened === 'first' && <Modal
                    modalText={'Are you sure you want to delete it?'}
                    title={'Do you want to delete this file?'}
                    buttonOnClick={() => this.hideModal()}
                    actions={{
                        cancel: () => <Button cStyles={this.styles.actionButton} text={'Close'} bgc={'darkred'} OnClick={() => this.hideModal} />,
                        ok: () => <Button cStyles={this.styles.actionButton} text={'Ok'} bgc={'darkred'} OnClick={() => this.hideModal} />,
                    }}
                />}
                {this.state.modalOpened === 'second' && <Modal
                    modalText={'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident'}
                    title={'Modal2'}
                    buttonOnClick={() => this.hideModal()}
                    closeBtn
                    actions={{
                        ok: () => <Button cStyles={this.styles.actionButton} text={'Ok'} bgc={'darkred'} OnClick={() => this.hideModal} />,
                    }}
                />}
            </div>
        );
    }
}

export default App;
