import React, {Component} from 'react';
import Products from "./components/Products/Products";
import axios from 'axios'

class App extends Component {

    state = {}


    getProducts = async () => {
        await axios.get('http://localhost:3000/products.json')
            .then(data => this.setState({products: data.data}))
    }

    componentDidMount() {
        this.getProducts()
    }

    openModal() {
        this.setState({modalOpened: true})
    }

    hideModal() {
        this.setState({modalOpened: false})
    }

    render() {
        return (
            <div>
                <Products products={this.state.products}/>
            </div>
        );
    }
}


export default App;
