import React, {Component} from 'react';
import Button from "../Button/Button";
import PropTypes from 'prop-types'

class Modal extends Component {
    styles = {
        modalWrapper: {
            backgroundColor: 'rgba(0,0,0,0.2)',
            position: 'fixed',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            zIndex: 1000
        },

        modal: {
            position: 'fixed',
            left: '50%',
            top: '50%',
            backgroundColor: 'white',
            transform: 'translate(-50%, -50%)',
            borderRadius: '10px',
            border: '1px solid darkgrey',
            zIndex: 1001
        },

        header: {
            display: 'flex',
            backgroundColor: 'darkgrey',
            justifyContent: 'space-between',
            padding: '3px 20px',
            color: 'white',
            borderTopLeftRadius: '10px',
            borderTopRightRadius: '10px'
        },

        modalBody: {
            padding: '30px',
            borderBottomLeftRadius: '10px',
            borderBottomRightRadius: '10px'
        },

        button: {
            padding: '1px',
            fontSize: '32px',
            color: 'white'
        },

        modalText: {
            color: '#3d3d3d'
        },

        modalFooter: {
            display: 'flex',
            justifyContent: 'center',
            padding: '5px'
        }
    }


    render() {
        const {modalText, title, buttonOnClick, closeBtn, actions} = this.props

        return (
            <div id={'wrapper'} style={this.styles.modalWrapper}>
                <div onClick={buttonOnClick} style={this.styles.modal}>
                    <div style={this.styles.header}>
                        <h3 style={this.styles.modalTitle}>{title}</h3>
                        {closeBtn &&
                        <Button text={'x'} bgc={'transparent'} cStyles={this.styles.button} OnClick={buttonOnClick}/>}
                    </div>
                    <div style={this.styles.modalBody}>
                        <div style={this.styles.modalText}>
                            {modalText}
                        </div>
                    </div>
                    <div style={this.styles.modalFooter}>
                        {actions.cancel && actions.cancel()}
                        {actions.ok && actions.ok()}
                    </div>
                </div>
            </div>
        );
    }
}

Modal.propTypes = {
    modalText: PropTypes.string,
    title: PropTypes.string,
    buttonOnClick: PropTypes.func,
    closeBtn: PropTypes.bool,
    actions: PropTypes.object
}

export default Modal;