import React, {Component} from 'react';
import Product from "../Product/Product";
import PropTypes from 'prop-types';

class Products extends Component {

    render() {
        const {products, onClick} = this.props

        return (
            <div>
                {products && products.map((item) => <Product onClick={onClick} item={item}/>)}
            </div>
        );
    }
}

Products.propTypes = {
    products: PropTypes.array,
    onClick: PropTypes.func
};

export default Products;