import React, {Component} from 'react';
import Favorite from "../Favorite/Favorite";
import Modal from "../Modal/Modal";
import PropTypes from 'prop-types';
import Button from "../Button/Button";


class Product extends Component {

    state = {
        modalOpened: false
    }

    styles = {
        wrap: {
            display: 'inline-flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
            width: '200px',
            border: '1px solid green',
            borderRadius: '5px',
            padding: '10px',
            margin: '15px',
            position: 'relative'
        },

        flex: {
            display: 'inline-flex'
        },

        img: {
            height: '150px'
        },

        svg: {
            width: '40px',
            height: '40px',
            position: 'absolute',
            top: '10px',
            right: '10px'
        },

        actionButton: {
            color: 'white',
            margin: '5px'
        }
    }

    addToCart() {
        const {item} = this.props
        let products = []

        if (localStorage.getItem('cart')) {
            products = JSON.parse(localStorage.getItem('cart'));
        }
        products.push(item);
        localStorage.setItem('cart', JSON.stringify(products))
    }

    openModal() {
        this.setState({modalOpened: true})
    }

    hideModal() {
        this.setState({modalOpened: false})
    }


    render() {
        const {item: {name, price, vendorCode, color, img}} = this.props
        return (
            <div style={this.styles.flex}>
                <div style={this.styles.wrap}>
                    <div>
                        <img style={this.styles.img} src={img} alt="img"/>
                    </div>
                    <Favorite item={{name, price, vendorCode, color}} style={this.styles.svg}/>
                    <h3>{name}</h3>
                    <span>price: {price}grn</span>
                    <span>vendor: {vendorCode}</span>
                    <span>color: {color}</span>
                    <button onClick={() => this.openModal()}>Add to cart</button>
                </div>
                <div>
                    {this.state.modalOpened && <Modal
                        modalText={`Добавить в корзину ${name}?`}
                        title={'Вопрос'}
                        buttonOnClick={() => this.hideModal()}
                        closeBtn
                        actions={{
                            cancel: () => <Button cStyles={this.styles.actionButton} text={'Close'} bgc={'darkred'} OnClick={() => this.hideModal} />,
                            ok: () => <Button cStyles={this.styles.actionButton} text={'Ok'} bgc={'darkgreen'} OnClick={() => this.addToCart({name, price, vendorCode, color, img})} />,
                        }}
                    />}
                </div>
            </div>
        );
    }
}

Product.propTypes = {
    item: PropTypes.object
};

export default Product;