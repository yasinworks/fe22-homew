import React, {Component} from 'react';
import './button.scss'

class Button extends Component {
    render() {
        const {text, OnClick, bgc, cStyles} = this.props
        return (
                <button className={'button'} style={{backgroundColor: bgc, ...cStyles}} onClick={OnClick}>{text}</button>
        );
    }
}


export default Button;