import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Favorite extends Component {
    state = {
        clicked: false
    }

    componentDidMount() {
        const {item} = this.props
        if (JSON.parse(localStorage.getItem('favorites'))) {
            let prodStorage = JSON.parse(localStorage.getItem('favorites'))
            prodStorage.forEach((i) => {
                if (i.vendorCode === item.vendorCode) {
                    this.setState({clicked: true})
                }
            })
        }
    }

    addFavorite() {
        const {item} = this.props
        const {clicked} = this.state
        let products = []

        if (clicked) {
            this.setState({clicked: false})
            let prodStorage = JSON.parse(localStorage.getItem('favorites'))
            let storage = prodStorage.filter((prod) => prod.vendorCode !== item.vendorCode)

            localStorage.setItem('favorites', JSON.stringify(storage));
        } else {
            this.setState({clicked: true})
            if (localStorage.getItem('favorites')) {
                products = JSON.parse(localStorage.getItem('favorites'));
            }
            products.push(item);
            localStorage.setItem('favorites', JSON.stringify(products));
        }
    }

    render() {
        const {style} = this.props
        const {clicked} = this.state

        return (
            <div onClick={() => this.addFavorite()}>
                <svg style={style} height="512pt" viewBox="0 0 512 512" width="512pt"
                     xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="m416 512h-320c-53.023438 0-96-42.976562-96-96v-320c0-53.023438 42.976562-96 96-96h320c53.023438 0 96 42.976562 96 96v320c0 53.023438-42.976562 96-96 96zm0 0"
                        fill="#fff9dd"/>
                    <path
                        d="m383.328125 226.03125c-1.679687-5.183594-6.289063-8.847656-11.695313-9.34375l-73.90625-6.71875-29.214843-68.367188c-2.144531-5.027343-7.054688-8.273437-12.511719-8.273437s-10.367188 3.246094-12.511719 8.273437l-29.214843 68.367188-73.90625 6.71875c-5.421876.496094-10.015626 4.160156-11.695313 9.34375s-.128906 10.863281 3.96875 14.464844l55.871094 48.976562-16.480469 72.542969c-1.214844 5.328125.863281 10.847656 5.28125 14.046875 2.367188 1.730469 5.167969 2.59375 7.984375 2.59375 2.398437 0 4.816406-.640625 6.976563-1.9375l63.726562-38.09375 63.710938 38.09375c4.671874 2.785156 10.546874 2.546875 14.960937-.65625 4.417969-3.199219 6.496094-8.71875 5.28125-14.046875l-16.480469-72.542969 55.871094-48.992187c4.113281-3.585938 5.664062-9.265625 3.984375-14.449219zm0 0"
                        fill={clicked ? "#ffd200" : "#fff"}/>
                </svg>
            </div>
        );
    }
}

Favorite.propTypes = {
    item: PropTypes.object,
    clicked: PropTypes.bool,
    style: PropTypes.object
};

export default Favorite;