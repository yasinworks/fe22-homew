const gulp = require('gulp')
const clean = require('gulp-clean')
const concat = require('gulp-concat')
const browserSync = require('browser-sync')
const minifyJs = require('gulp-js-minify');
const minifyCss = require('gulp-clean-css');
const sass = require('gulp-sass');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify')

sass.compiler = require('node-sass');


const path = {
    build: {
      root: './dist',
      css: './dist/style',
      js: './dist/script',
      img: './dist/img'
    },

    src: {
        css: './src/style/**/*.scss',
        js: './src/script/*.js',
        img: './src/img/**/*',
        html: './index.html'
    }
}

// functions

const cleanBuild = () => (
    gulp.src(path.build.root, {allowEmpty: true})
        .pipe(clean())
)

const buildStyle = () => (
    gulp.src(path.src.css)
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe(gulp.dest(path.build.css))
        .pipe(browserSync.stream())
)

const buildJs = () => (
    gulp.src(path.src.js)
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(minifyJs())
        .pipe(gulp.dest(path.build.js))
        .pipe(browserSync.stream())
)

const buildImg = () => (
    gulp.src(path.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(path.build.img))
)

const devServer = () => (
    browserSync.init({
        server: {
            baseDir: './'
        },
        online: true,
        tunnel: true,
    })
)

const watch = () => {
    devServer()
    gulp.watch(path.src.css, buildStyle).on('change', browserSync.reload)
    gulp.watch(path.src.js, buildJs).on('change', browserSync.reload)
    gulp.watch(path.src.img, buildImg).on('change', browserSync.reload)
    gulp.watch(path.src.html).on('change', browserSync.reload)
}

// tasks


// gulp.task('minify-js', )

gulp.task('build', gulp.series(
    cleanBuild,
    buildStyle,
    buildJs,
    buildImg
))


gulp.task('dev', gulp.series(
    watch
))

gulp.task('style', buildStyle)